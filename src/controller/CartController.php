<?php


namespace controller;

class CartController
{

    public static function cart():void{

        // Variables à transmettre à la vue
        $params = [
            "title"  => "Panier",
            "module" => "cart.php"
        ];

        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);
    }

    public static function addToCart():void{

        $article = $_POST['idProduct'];

        if($_SESSION['cart'][$article] != null){
            $_SESSION['cart'][$article]['cartQuantity'] += $_POST['quantity-add'];
            $_SESSION['cartPrice'] += $_POST['price-add'];
        }else if(!isset($_SESSION['cart'][$article])){
            $_SESSION['cart'][$article] = [
                "cartQuantity" => $_POST['quantity-add'],
                "unitPrice" => $_POST['unitPrice'],
                "productImg" => $_POST['imgProduct'],
                "productName" => $_POST['nameProduct'],
                "productCategory" => $_POST['categoryProduct']
            ];

            $_SESSION['cartPrice'] += $_POST['price-add'];
        }

        header('Location: /cart');
    }
}