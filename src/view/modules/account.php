
<?php if($_GET['status'] === 'signin_success') : ?>
    <div class="box info" style="margin: 30px">Inscription réussie ! Vous pouvez dès à présent vous connecter.</div>
<?php endif ?>

<?php if($_GET['status'] === 'signin_fail') : ?>
    <div class="box error" style="margin: 30px">Inscription échouée, veuillez réessayer.</div>
<?php endif ?>

<?php if($_GET['status'] === 'login_fail') : ?>
    <div class="box error" style="margin: 30px">La connexion a échoué. Vérifiez vos identifiants et réessayez.</div>
<?php endif ?>

<?php if($_GET['status'] === 'logout') : ?>
    <div class="box info" style="margin: 30px">Vous êtes déconnecté. A bientôt !</div>
<?php endif ?>

<div id="account">

<form class="account-login" method="post" action="/account/login">

  <h2>Connexion</h2>
  <h3>Tu as déjà un compte ?</h3>

  <p>Adresse mail</p>
  <input type="text" name="usermail" placeholder="Adresse mail" />

  <p>Mot de passe</p>
  <input type="password" name="userpass" placeholder="Mot de passe" />

  <input type="submit" value="Connexion" />

</form>

<form class="account-signin" method="post" action="/account/signin">

  <h2>Inscription</h2>
  <h3>Crée ton compte rapidement en remplissant le formulaire ci-dessous.</h3>

  <p>Nom</p>
  <input type="text" name="userlastname" placeholder="Nom" id="lastname"/>

  <p>Prénom</p>
  <input type="text" name="userfirstname" placeholder="Prénom" id="firstname" />

  <p>Adresse mail</p>
  <input type="text" name="usermail" placeholder="Adresse mail" id="mail"/>

  <p>Mot de passe</p>
  <input type="password" name="userpass" placeholder="Mot de passe" id="pass"/>

  <p>Répéter le mot de passe</p>
  <input type="password" name="userpass" placeholder="Mot de passe" id="pass-verif"/>

  <input type="submit" value="Inscription" />

</form>

</div>

<script src="/public/scripts/signin.js"></script>
