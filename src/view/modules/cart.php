<div id="cart">

    <h2>Panier</h2>

    <?php if(empty($_SESSION['cart'])) :?>
    <p>Votre panier est vide</p>

    <?php else : ?>

    <?php foreach ($_SESSION['cart'] as $c) {  ?>

            <div class="container">

                <img src=" public/images/<?php echo $c['productImg'] ?>">

                <div class="small-container">
                    <p class="cart-category"><?php echo $c['productCategory'] ?></p>
                    <h1 class="cartProductName"><?php  echo $c['productName'] ?></h1>
                </div>

                <div class="small-container">
                    <p>Quantité :</p>
                    <div style="display: flex">
                        <button type="button" id="lower-cart" class="cart-btn">-</button>
                        <button type="button" id="quantity-cart" class="cart-btn" value="<?php echo $c['cartQuantity'] ?>"><?php echo $c['cartQuantity'] ?></button>
                        <button type="button" id="upper-cart" class="cart-btn">+</button>
                    </div>
                </div>

                <div class="small-container">
                    <p>Prix unitaire : </p>
                    <p><?php echo $c['unitPrice'] ?>€</p>
                </div>

            </div>
        <?php } ?>

    <p class="cart-totalPrice">Prix total du panier :</p>
    <p class="cart-totalPrice"><?php echo $_SESSION['cartPrice'] ?>€</p>

    <a class="paiementBtn"> Procéder au paiement </a>

    <?php endif ?>
</div>
