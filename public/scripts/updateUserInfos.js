document.addEventListener('DOMContentLoaded', function () {

/*****************************MODIFICATION DES USER-INFOS************************/

    let userInfoFirstname = document.getElementById("userfirstname-infos")
    let userInfosLastname = document.getElementById("userlastname-infos")
    let userInfosMail = document.getElementById("usermail-infos")

    const regex = new RegExp(/\S+@\S+\.\S+/)

    userInfosLastname.addEventListener('change',function (){
        if(userInfosLastname.value.length < 2){
            userInfosLastname.className = 'invalid'
            userInfosLastname.previousElementSibling.className = 'invalid'
        }
        else if(userInfosLastname.value.length >= 2){
            userInfosLastname.className = 'valid'
            userInfosLastname.previousElementSibling.className = 'valid'
        }
    })

    userInfoFirstname.addEventListener('change',function (){
        if(userInfoFirstname.value.length < 2){
            userInfoFirstname.className = 'invalid'
            userInfoFirstname.previousElementSibling.className = 'invalid'
        }
        else if (userInfoFirstname.value.length >= 2){
            userInfoFirstname.className = 'valid'
            userInfoFirstname.previousElementSibling.className = 'valid'
        }
    })

    userInfosMail.addEventListener('change',function (){
        if(regex.test(userInfosMail.value)){
            userInfosMail.className = 'valid'
            userInfosMail.previousElementSibling.className = 'valid'
        }
        else if(!regex.test(userInfosMail)){
            userInfosMail.className = 'invalid'
            userInfosMail.previousElementSibling.className = 'invalid'
        }
    })

})