<?php

namespace view;

class Template {
  static function render($params) {
    if($params["module"] === "StarWars.php") : ?>

        <!DOCTYPE html>
        <html lang="en">

        <head>
            <meta charset="UTF-8">
            <title>The Project Awakens</title>
            <link rel="stylesheet" href="public/styles/StarWars.css"/>
        </head>

        <body id="StarWars">

        <div class="starwars-demo">
            <img src="public/images/star.svg" alt="Star" class="star">
            <img src="public/images/wars.svg" alt="Wars" class="wars">
            <h2 class="byline" id="byline">The Project Awakens</h2>
            <a href="/home">Clique sur ce lien Luke...</a>
        </div>

        </body>

        <script src="/public/scripts/StarWars.js"></script>
        </html>


    <?php else : ?>

<!DOCTYPE html>
<html>

<head>

  <!-- Inclusion des méta-données -->
  <?php include "commons/head.php" ?>

</head>

<body>

  <!-- TODO: Inclure le nav ici -->
  <?php include "commons/nav.php" ?>

  <!-- Inclusion du module à afficher -->
  <?php include "modules/" . $params["module"]; ?>

  <!-- TODO: Inclure le footer ici -->
  <?php include "commons/footer.php" ?>

</body>

</html>

<?php endif ?>
<?php
  }
}