document.addEventListener('DOMContentLoaded', function () {


/***************************GESTION DE L'INSCRIPTION**************************/

   let userlastname = document.getElementById('lastname')
    let userfirstname = document.getElementById('firstname')
    let usermail = document.getElementById('mail')
    let userpass = document.getElementById('pass')
    let userpassVerif = document.getElementById('pass-verif')

    const regex = new RegExp(/\S+@\S+\.\S+/)

    userlastname.addEventListener('change',function (){
        if(userlastname.value.length < 2){
            userlastname.className = 'invalid'
            userlastname.previousElementSibling.className = 'invalid'
        }
        else if(userlastname.value.length >= 2){
            userlastname.className = 'valid'
            userlastname.previousElementSibling.className = 'valid'
        }
    })

    userfirstname.addEventListener('change',function (){
        if(userfirstname.value.length < 2){
            userfirstname.className = 'invalid'
            userfirstname.previousElementSibling.className = 'invalid'
        }
        else if (userfirstname.value.length >= 2){
            userfirstname.className = 'valid'
            userfirstname.previousElementSibling.className = 'valid'
        }
    })

    usermail.addEventListener('change',function (){
        if(regex.test(usermail.value)){
            usermail.className = 'valid'
            usermail.previousElementSibling.className = 'valid'
        }
        else if(!regex.test(usermail)){
            usermail.className = 'invalid'
            usermail.previousElementSibling.className = 'invalid'
        }
    })


    userpass.addEventListener('change',function(){
        if(userpass.value.length < 6){
            userpass.className = 'invalid'
            userpass.previousElementSibling.className = 'invalid'
        }
        else if (userpass.value.length >= 6){
            userpass.className = 'valid'
            userpass.previousElementSibling.className = 'valid'
        }
    })

    userpassVerif.addEventListener('change',function (){
        if(userpass.value === userpassVerif.value){
            userpassVerif.className = 'valid'
            userpassVerif.previousElementSibling.className = 'valid'
        }
        else if(userpass.value !== userpassVerif.value){
            userpassVerif.className = 'invalid'
            userpassVerif.previousElementSibling.className = 'invalid'
        }
    })
});