<?php
error_reporting(0);
$firstname = $_SESSION['firstname'];
$lastname = $_SESSION['lastname'];
$connected = $_SESSION['connected'];
?>

<nav>
    <img src="/public/images/logo.jpeg">
    <a href="/home">Accueil</a>
    <a href="/store">Boutique</a>

    <?php if(!$connected) : ?>
    <a class="account" href="/account">Compte<img src="/public/images/avatar.png"></a>
    <?php else :?>
    <a class="account" href="/account/infos"><?php echo $firstname . " " . $lastname ?><img src="/public/images/avatar.png"></a>
    <a href="/cart">Panier</a>
    <a href="/account/logout">Déconnexion</a>
    <?php endif ?>
</nav>


