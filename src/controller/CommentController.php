<?php


namespace controller;


class CommentController
{
    public static function postComment():void{

        if($_SESSION['connected']){
            $comment = htmlspecialchars($_POST['comment']);

            \model\CommentModel::insertComment( $_SESSION['id'],$_POST['id_product'],$comment,date('d-m-y'));
        }

        header('Location: /store/' . $_POST['id_product'] );
        exit;
    }
}