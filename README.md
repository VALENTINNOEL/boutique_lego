# L2 Informatique - Projet Web

## Description

Développement d'une application simplifiée de e-commerce.

## Auteur

Noël Valentin

## Liens utiles

- [Sujet](https://florian-lepretre.herokuapp.com/teaching/projetweb/sujet/)
- [Dépôts des étudiants](https://docs.google.com/spreadsheets/d/16ydvylkxeVydqQASeoj1vYBUgb6zJ1EWZZEMHhHzSQ8/edit?usp=sharing)

##Lien de mon site

http://projet-web-ulco-l2-valentinn.herokuapp.com