<?php

/** Initialisation de l'autoloading et du router ******************************/

require('src/Autoloader.php');
Autoloader::register();

session_start();

$router = new router\Router(basename(__DIR__));

/** Définition des routes *****************************************************/

// GET "/"
$router->get('/home', 'controller\IndexController@index');
$router->get('/', 'controller\IndexController@indexStarWars');


// Erreur 404
$router->whenNotFound('controller\ErrorController@error');

//Route Store
$router->get('/store', 'controller\StoreController@store');

//Route numéro de produit
$router->get('/store/{:num}', 'controller\StoreController@product');

//Route account
$router->get('/account','controller\AccountController@account');

//Route login
$router->post('/account/login','controller\AccountController@login');

//Route signin
$router->post('/account/signin','controller\AccountController@signin');

//Route logout
$router->get('/account/logout','controller\AccountController@logout');

//Route avis
$router->post('/postComment','controller\CommentController@postComment');

//Route filtre et tri
$router->post('/search','controller\StoreController@search');

//Route infos du compte
$router->get('/account/infos','controller\AccountController@infos');

//Route update des infos utilisateur
$router->post('/account/update','controller\AccountController@updateInfos');

//Route panier
$router->get('/cart','controller\CartController@cart');
$router->post('/cart/add','controller\CartController@addToCart');

/** Ecoute des requêtes entrantes *********************************************/

$router->listen();
