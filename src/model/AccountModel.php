<?php

namespace model;

class AccountModel
{

    public static function check($firstname, $lastname, $mail, $password):bool{

        $verifEmail1 = true;
        $verifEmail2 = true;

        // Connexion à la base de données
        $db = \model\Model::connect();

        // Requête SQL
        $sql = "SELECT mail FROM account WHERE mail = ?" ;

        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute(array($mail));

        // Retourner les résultats (type array)
        $reqMail= $req->fetchAll();

        $params =[
            "mail" => $reqMail
        ];

        if($params["mail"] != null) $verifEmail1 = false;
        if(!filter_var($mail,FILTER_VALIDATE_EMAIL)) $verifEmail2 = false;

        if(strlen($firstname) >= 2 && strlen($lastname) >= 2 && strlen($password) >= 6 && $verifEmail1 && $verifEmail2) return true;
        else return false;
    }

    public static function signin($firstname, $lastname, $mail, $password):bool{

        if(\model\AccountModel::check($firstname, $lastname, $mail, $password)){

            // Connexion à la base de données
            $db = \model\Model::connect();

            // Requête SQL
            $sql = "INSERT INTO account(firstname,lastname,mail,password)
                    VALUES ('$firstname','$lastname','$mail','$password')";

            // Exécution de la requête
            $req = $db->prepare($sql);
            $req->execute();

            return true;
        }else return false;
    }

    public static function login($mail, $password):array{

        // Connexion à la base de données
        $db = \model\Model::connect();

        // Requête SQL
        $sql = "SELECT * FROM account WHERE mail = ? && password = ?";

        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute(array($mail,$password));

        // Retourner les résultats (type array)
        return $req->fetchAll();
    }

    public static function updateInfosCheck($firstname,$lastname,$mail):bool{

        $verifEmail1 = true;
        $verifEmail2 = true;
        $verifLF = true;

        if($mail != null) {
            // Connexion à la base de données
            $db = \model\Model::connect();

            // Requête SQL
            $sql = "SELECT mail FROM account WHERE mail = ?";

            // Exécution de la requête
            $req = $db->prepare($sql);
            $req->execute(array($mail));

            // Retourner les résultats (type array)
            $reqMail = $req->fetchAll();

            $params = [
                "mail" => $reqMail
            ];


            if ($params["mail"] != null) $verifEmail1 = false;
            if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) $verifEmail2 = false;
        }

        if(strlen($firstname) < 2 || strlen($lastname) < 2) $verifLF = false;

        if($verifLF && $verifEmail1 && $verifEmail2) return true;

        else return false;
    }


    public static function updateInfos($firstname,$lastname,$mail,$id):bool{

        if(\model\AccountModel::updateInfosCheck($firstname,$lastname,$mail)) {

            // Connexion à la base de données
            $db = \model\Model::connect();

            if ($mail == null){

                // Requête SQL
                $sql = "UPDATE account
                    SET firstname = ?,
                    lastname = ?
                    WHERE id = ?";
            }
            else{

                $sql_mail = " mail = " . "'".$mail."'";

                // Requête SQL
                $sql = "UPDATE account
                    SET firstname = ?,
                    lastname = ?," . $sql_mail ." 
                    WHERE id = ?";
            }

            // Exécution de la requête
            $req = $db->prepare($sql);
            $req->execute(array($firstname,$lastname,$id));

            return true;
        }
        else return false;
    }
}