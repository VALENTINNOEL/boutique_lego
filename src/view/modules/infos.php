<div id="user-infos">

    <?php if($_GET['status'] === 'update_success') :?>
        <div class="box info">Vos informations ont été bien mises à jour.</div>
    <?php endif ?>

    <h2>Information du compte</h2>
    <h3>Informations personnelles</h3>

    <form method="post" action="/account/update">

        <p>Prénom</p>
        <input type="text" name="userfirstname-infos" value="<?=  $_SESSION['firstname']  ?>" id="userfirstname-infos">

        <p>Nom</p>
        <input type="text" name="userlastname-infos" value="<?=  $_SESSION['lastname']  ?>" id="userlastname-infos">

        <p>Adresse mail</p>
        <input type="text" name="usermail-infos" value="<?=  $_SESSION['mail']  ?>" id="usermail-infos">

        <input type="submit" value="Modifier mes informations">

    </form>

    <h3>Commandes</h3>
    <p>Tu n'as pas de commande en cours.</p>

</div>

<script src="/public/scripts/updateUserInfos.js"></script>
