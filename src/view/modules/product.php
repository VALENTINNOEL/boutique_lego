<div id="product">

    <div>
        <?php foreach ($params["specs"] as $s) { ?>
        <div class="product-images">
            <img src="/public/images/<?= $s["P_img"] ?>" id="main-img">
            <div class="product-miniatures" id="img-alt">
                <div><img src="/public/images/<?= $s["P_img"] ?>"></div>
                <div><img src="/public/images/<?= $s["P_img1"] ?>"></div>
                <div><img src="/public/images/<?= $s["P_img2"] ?>"></div>
                <div><img src="/public/images/<?= $s["P_img3"] ?>"></div>
            </div>
        </div>

        <div class="product-infos" id="p-infos">
            <p class="product-category"><?= $s["C_name"] ?></p>
            <h1><?= $s["P_name"] ?></h1>
            <p class="product-price" id="product price"><?= $s["P_price"] ?>€</p>


            <?php if($_SESSION['connected']) : ?>

            <form method="post" action="/cart/add">
                <button type="button" id="lower">-</button>
                <button type="button" id="quantity">1</button>
                <button type="button" id="upper">+</button>

                <input type="hidden" id="quantity-add" value="1" name="quantity-add">
                <input type="hidden" id="price-add" value="<?= $s["P_price"] ?>" name="price-add">
                <input type="hidden" id="unitPrice" value="<?= $s["P_price"] ?>" name="unitPrice">
                <input type="hidden" name="imgProduct" value="<?= $s["P_img"] ?>">
                <input type="hidden" name="categoryProduct" value="<?= $s["C_name"] ?>">
                <input type="hidden" name="nameProduct" value="<?= $s["P_name"] ?>">
                <input type="hidden" name="idProduct" value="<?= $params["title"] ?>">


                <input type="submit" value="Ajouter au panier">
            <?php else : ?>

                <button> <a href="/account">Pour acheter un article, connectez vous.</a></button>
            <?php endif ?>
            </form>
        </div>
        <?php } ?>
    </div>

    <div>
        <div class="product-spec">
            <h2>Specificités</h2>
            <?= $s["P_spec"] ?>
        </div>
        <div class="product-comments">
            <h2>Avis</h2>
            <?php foreach ($params["comment"] as $c) { ?>
            <p class="product-comment-author"><?= $c["A_fname"] . " " . $c["A_lname"] ?></p>
            <p style="font-style: italic; margin-bottom: 10px"><?= $c["C_content"] ?></p>
                <hr>
            <?php } ?>

            <form method="post" action="/postComment">

                <?php if($_SESSION['connected']) : ?>
                <input type="text" name="comment" placeholder="Rédiger un commentaire">
                <input type="hidden" name="id_product" value="<?= $params["title"] ?>">

                <input type="submit" value="Avis" accesskey="enter" style="display: none"/>
                <?php else : ?>

               <button> <a href="/account">Pour laisser un avis, connectez vous.</a></button>
                <?php endif ?>

            </form>
        </div>
    </div>

</div>

<script src="/public/scripts/product.js"></script>

