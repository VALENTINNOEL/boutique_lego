<?php

namespace controller;

class StoreController {

  public function store(): void
  {
    // Communications avec la base de données
    $categories = \model\StoreModel::listCategories();
    $products = \model\StoreModel::listProducts();

    // Variables à transmettre à la vue
    $params = array(
      "title" => "Store",
      "module" => "store.php",
      "categories" => $categories,
        "products" => $products
    );

    // Faire le rendu de la vue "src/view/Template.php"
    \view\Template::render($params);
  }

  public function product(int $id):void{

      // Communications avec la base de données
      $specs = \model\StoreModel::infoProduct($id);
      $comment = \model\CommentModel::listComment($id);

      // Variables à transmettre à la vue
      $params = array(
          "title" => $id,
          "module" => "product.php",
          "specs" => $specs,
          "comment" => $comment
          );

      if($params['specs'] == null) {
          header('Location: /store');
          exit;
      }else  \view\Template::render($params);    // Faire le rendu de la vue "src/view/Template.php"
  }

  public static function search():void{

      if(isset($_POST['order'])){
          if(($_POST['order'] === 'croissant')) $order = 'ASC';
          else if($_POST['order'] === 'decroissant') $order = 'DESC';
      }else $order = null;

      if(isset($_POST['category']))$category = $_POST['category'];
      else $category = null;

      if(isset($_POST['search']))$search = htmlspecialchars($_POST['search']);
      else $search = null;

      // Communications avec la base de données
      $products = \model\StoreModel::listProducts($order,$category,$search);
      $categories = \model\StoreModel::listCategories();


      // Variables à transmettre à la vue
      $params = array(
          "title" => "Store",
          "module" => "store.php",
          "categories" => $categories,
          "products" => $products
      );

      // Faire le rendu de la vue "src/view/Template.php"
      \view\Template::render($params);
  }
}