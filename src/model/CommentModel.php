<?php


namespace model;


class CommentModel
{

    public static function insertComment($id_account,$id_product,$content,$date):void{

        // Connexion à la base de données
        $db = \model\Model::connect();

        // Requête SQL
        $sql = "INSERT INTO comment(content,date,id_product,id_account)
                    VALUES (?, ?, ?, ?)";

        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute(array($content, $date, $id_product, $id_account));
    }

    public static function listComment($id):array{

        // Connexion à la base de données
        $db = \model\Model::connect();

        // Requête SQL
        $sql = "SELECT C.content AS C_content,A.firstname AS A_fname, A.lastname AS A_lname FROM comment C
                    INNER JOIN account A
                    ON C.id_account = A.id WHERE C.id_product = ?";

        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute(array($id));

        // Retourner les résultats (type array)
        return $req->fetchAll();
    }

}