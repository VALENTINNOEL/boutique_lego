document.addEventListener('DOMContentLoaded', function () {

    /**********************************GESTION DES BOUTONS ************************************/

    let lowerBtn = document.getElementById('lower')
    let upperBtn = document.getElementById('upper')
    let quantity = document.getElementById('quantity')
    let warning = document.getElementById("p-infos")

    let quantityAdd = document.getElementById("quantity-add")
    let price_add = document.getElementById("price-add")
    let initialPrice = price_add.value

    let count = 1

    upperBtn.addEventListener('click',function(){
        lowerBtn.disabled = false
        if(count == 4){
            count++
            quantity.innerHTML = count.toString()
            upperBtn.disabled = true
            let error = document.createElement("div")
            error.className = "box error"
            error.innerText = "Quantité maximale autorisée"
            warning.appendChild(error)

            quantityAdd.value = count
            price_add.value = initialPrice*count
        }else{
            count++
            quantity.innerText = count.toString()

            quantityAdd.value = count
            price_add.value = initialPrice*count
        }
    })

    lowerBtn.addEventListener('click',function(){
        if(count == 5){
            upperBtn.disabled = false
            count--
            quantity.innerHTML = count.toString()
            warning.removeChild(warning.lastChild)

            quantityAdd.value = count
            price_add.value = initialPrice*count
        }else if(count == 1){
            quantity.innerHTML = count.toString()
            lowerBtn.disabled = true

            quantityAdd.value = count
            price_add.value = initialPrice*count
        }
        else{
            count--
            quantity.innerHTML = count.toString()

            quantityAdd.value = count
            price_add.value = initialPrice*count
        }
    })


    /*********************************GESTION DES IMAGES************************************/

    let mainImg = document.getElementById('main-img')
    let divImgAlt = document.getElementById('img-alt')
    let imgAlt = divImgAlt.getElementsByTagName('img')


    for(let i in imgAlt){
        imgAlt[i].addEventListener('click',function(){
            mainImg.src = imgAlt[i].src.toString()
        })
    }
})
