<?php

namespace model;

class StoreModel {

  static function listCategories(): array
  {

    // Connexion à la base de données
    $db = \model\Model::connect();

    // Requête SQL
    $sql = "SELECT id, name FROM category";
    
    // Exécution de la requête
    $req = $db->prepare($sql);
    $req->execute();

    // Retourner les résultats (type array)
    return $req->fetchAll();

  }

    public static function listProducts($order=null,?array $category=null,$search=null):array{

      if($order != null) $sql_order = ' ORDER BY product.price ' . $order;
      else $sql_order = null;

      if($category != null){
              $sql_category = ' WHERE ( category.name = ' . '"'.$category[0].'"';
              for($i = 1; $i < count($category); $i++){
                  $sql_category .= ' OR category.name = '. '"'.$category[$i].'"';
              }
              $sql_category .= ' )';
      }
      else $sql_category = null;

      if($search != null && $sql_category != null) $sql_search = ' && product.name LIKE ' . '"%'.$search.'%"';
      else if($search != null && $sql_category == null)$sql_search = ' WHERE product.name LIKE ' . '"%'.$search.'%"';
      else $sql_search = null;

        // Connexion à la base de données
        $db = \model\Model::connect();

        // Requête SQL
        $sql = "SELECT product.id AS P_id,
                 product.name AS P_name,
                 price AS P_price,
                 image AS P_image,
                 category.name AS C_name
                 FROM product
                 INNER JOIN category ON category.id = product.category " . $sql_category . $sql_search . $sql_order;

        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute();

        // Retourner les résultats (type array)
        return $req->fetchAll();
    }

    public static function infoProduct(int $id):array{
        // Connexion à la base de données
        $db = \model\Model::connect();

        // Requête SQL
        $sql = "SELECT P.name AS P_name,
                P.price AS P_price,
                P.image AS P_img,
                P.image_alt1 AS P_img1,
                P.image_alt2  AS P_img2,
                P.image_alt3  AS P_img3,
                P.spec AS P_spec,
                C.name AS C_name
                FROM product P
                INNER JOIN category C ON P.category = C.id  WHERE ? = P.id";

        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute(array($id));

        // Retourner les résultats (type array)
        return $req->fetchAll();
    }

}