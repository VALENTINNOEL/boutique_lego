<?php

namespace controller;

class AccountController{

    public static function account():void{

        // Variables à transmettre à la vue
        $params = [
            "title"  => "Account",
            "module" => "account.php"
        ];

        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);

    }

    public static function signin():void{

        $firstname = htmlspecialchars($_POST['userfirstname']);
        $lastname = htmlspecialchars($_POST['userlastname']);
        $mail = htmlspecialchars($_POST['usermail']);
        $password = htmlspecialchars($_POST['userpass']);

       $signin = \model\AccountModel::signin($firstname, $lastname, $mail, $password);

       if($signin){
           header('Location: /account?status=signin_success');
           exit;
       }else {
           header('Location: /account?status=signin_fail');
           exit;
       }
    }

    public static function login():void{

        $mail = htmlspecialchars($_POST['usermail']);
        $password = htmlspecialchars($_POST['userpass']);

        $user = \model\AccountModel::login($mail,$password);

        $params = array(
            "user" => $user
        );


        if($params['user'] != null){

            $_SESSION['id'] = $params['user'][0]['id'];
            $_SESSION['firstname'] = $params['user'][0]['firstname'];
            $_SESSION['lastname'] = $params['user'][0]['lastname'];
            $_SESSION['mail'] = $params['user'][0]['mail'];
            $_SESSION['connected'] = true;

            header('Location: /store');
            exit();
        }else{
            header('Location: /account?status=login_fail');
            exit();
        }
    }

    public static function logout():void{
        session_destroy();
        header('Location: /account?status=logout');
        exit;
    }

    public static function infos():void{

        // Variables à transmettre à la vue
        $params = [
            "title"  => "Information du compte",
            "module" => "infos.php"
        ];

        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);
    }

    public static function updateInfos():void{

        $firstname = htmlspecialchars($_POST['userfirstname-infos']);
        $lastname = htmlspecialchars($_POST['userlastname-infos']);

        $mail = htmlspecialchars($_POST['usermail-infos']);
        if($mail === $_SESSION['mail']) $mail = null;

        $userInfos = \model\AccountModel::updateInfos($firstname,$lastname,$mail,$_SESSION['id']);

        if($userInfos){

            $_SESSION['firstname'] =  $firstname;
            $_SESSION['lastname'] =  $lastname;
            $_SESSION['mail'] =  $_POST['usermail-infos'];

            header('Location: /account/infos?status=update_success');
            exit();
        }else{
            header('Location: /account/infos');
            exit();
        }


    }
}
